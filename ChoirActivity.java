package com.candaceyoung.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.candaceyoung.app.R;
import com.candaceyoung.app.RetrofitApi.ApiClient;
import com.candaceyoung.app.adapters.ChoirAdapter;
import com.candaceyoung.app.interfaces.ApiInterface;
import com.candaceyoung.app.model.DescriptionModel;
import com.candaceyoung.app.model.LogoutModel;
import com.candaceyoung.app.utils.CandaceyoungPreferences;
import com.candaceyoung.app.utils.SimpleSideDrawer;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChoirActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ChoirActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ChoirActivity.this;

    /*
     * Widgets
     * */
    TextView txtAnnTV;
    TextView txtAdminTV;
    TextView txtHomeTV;
    TextView txtChoirTV;
    LinearLayout homeLL;
    TextView txtWomensTV;
    TextView txtMensTV;
    LinearLayout annLL;
    LinearLayout choirLL;
    LinearLayout womensLL;
    LinearLayout mensLL;
    LinearLayout adminRL;
    LinearLayout profileLL;
    ImageView imgProfileIV;
    TextView txtEmailTV;
    TextView txtUsernameTV;
    LinearLayout teenLL;
    TextView txtTeenTV;
    LinearLayout donationsLL;
    TextView txtDonationsTV;
    LinearLayout gloryLL;
    TextView txtGloryTV;
    LinearLayout contactLL;
    TextView txtContactTV;

    @BindView(R.id.txtHeadingTV)
    TextView txtHeadingTV;

    @BindView(R.id.searchRL)
    RelativeLayout searchRL;
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;

    @BindView(R.id.imgCancelIV)
    ImageView imgCancelIV;

    @BindView(R.id.imgSearchIV)
    ImageView imgSearchIV;

    @BindView(R.id.imgEditIV)
    ImageView imgEditIV;

    @BindView(R.id.choirRV)
    RecyclerView choirRV;
    ChoirAdapter mChoirAdapter;
    List<DescriptionModel.Datum> mDescCategoryList;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;

    public SimpleSideDrawer mSimpleSideDrawer;
    LinearLayout logoutLL;
    TextView txtLogoutTV;
    List mRecentItemsList;
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    View adminView;
    @BindView(R.id.content)
    RelativeLayout content;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.navBar)
    NavigationView navBar;
    LinearLayout aboutLL;
    TextView txtAboutTV;
    LinearLayout upcomingEventsLL;
    TextView txtEventsTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choir);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        txtHeadingTV.setText(R.string.choir_rehearsal);
//set navigation
        setNavigationDrawer();
        setNavigationViewListener();
        setProfileData();
         if (CandaceyoungPreferences.readString(mActivity,CandaceyoungPreferences.ROLE,"").equals("0")){
            Log.e(TAG,"rolee"+CandaceyoungPreferences.readString(mActivity,CandaceyoungPreferences.ROLE,""));
            txtAdminTV.setVisibility(View.GONE);
            adminView.setVisibility(View.GONE);
        }
        else {
            txtAdminTV.setVisibility(View.VISIBLE);
            adminView.setVisibility(View.VISIBLE);
        }
        if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.HOME_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        } else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.CHOIR_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        } else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.ANN_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        } else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.MENS_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        } else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.WOMEN_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        } else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.PROFILE_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        } else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.ADMIN_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        } else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.LOGOUT_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        }else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.GLORY_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        }else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.TEEN_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        }else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.CONTACT_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        }else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.DONATION_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        }else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.ABOUT_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBg));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));
        }else if (CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.EVENT_SEL, false)) {
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBg));
        }
        getChoirList();

    }

    private void setProfileData() {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(mActivity)
                .load(CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.IMAGE, null))
                .apply(options)
                .into(imgProfileIV);
        txtUsernameTV.setText((CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.FIRST_NAME, null))+" "+(CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.LAST_NAME, null)));  txtEmailTV.setText(CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.EMAIL, null));
    }

    /*
             Execute api
              */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("c_id", "2");
        mMap.put("search", "");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getChoirList() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetChoirDetailsApi();
        }
    }

    private void executeGetChoirDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.descriptionRequest(mParam()).enqueue(new Callback<DescriptionModel>() {
            @Override
            public void onResponse(Call<DescriptionModel> call, Response<DescriptionModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                DescriptionModel mModel = response.body();
                mDescCategoryList = new ArrayList<>();
                mDescCategoryList = mModel.getData();
                if (mModel.getStatus() == 1) {
                    setChoirAdapter();
                    txtNoDataFountTV.setVisibility(View.GONE);
                } else {
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<DescriptionModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void setChoirAdapter() {
        mChoirAdapter = new ChoirAdapter(mActivity, mDescCategoryList);
        choirRV.setLayoutManager(new LinearLayoutManager(mActivity));
        choirRV.setAdapter(mChoirAdapter);
        mChoirAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    private void setNavigationDrawer() {
        View header = navBar.getHeaderView(0);
        homeLL = header.findViewById(R.id.homeLL);
        profileLL = header.findViewById(R.id.profileLL);
        annLL = header.findViewById(R.id.annLL);
        choirLL = header.findViewById(R.id.choirLL);
        womensLL = header.findViewById(R.id.womensLL);
        mensLL = header.findViewById(R.id.mensLL);
        txtHomeTV = header.findViewById(R.id.txtHomeTV);
        txtChoirTV = header.findViewById(R.id.txtChoirTV);
        txtAnnTV = header.findViewById(R.id.txtAnnTV);
        txtWomensTV = header.findViewById(R.id.txtWomensTV);
        txtMensTV = header.findViewById(R.id.txtMensTV);
        adminRL = header.findViewById(R.id.adminRL);
        imgProfileIV = header.findViewById(R.id.imgProfileIV);
        txtEmailTV = header.findViewById(R.id.txtEmailTV);
        txtUsernameTV = header.findViewById(R.id.txtUsernameTV);
        txtAdminTV = header.findViewById(R.id.txtAdminTV);
        logoutLL = header.findViewById(R.id.logoutLL);
        txtLogoutTV = header.findViewById(R.id.txtLogoutTV);
        adminView = header.findViewById(R.id.adminView);
        teenLL = header.findViewById(R.id.teenLL);
        txtTeenTV = header.findViewById(R.id.txtTeenTV);
        donationsLL = header.findViewById(R.id.donationsLL);
        txtDonationsTV = header.findViewById(R.id.txtDonationsTV);
        contactLL = header.findViewById(R.id.contactLL);
        txtContactTV = header.findViewById(R.id.txtContactTV);
        gloryLL = header.findViewById(R.id.gloryLL);
        txtGloryTV = header.findViewById(R.id.txtGloryTV);
        txtAboutTV = header.findViewById(R.id.txtAboutTV);
        aboutLL = header.findViewById(R.id.aboutLL);
        txtEventsTV = header.findViewById(R.id.txtEventsTV);
        upcomingEventsLL = header.findViewById(R.id.upcomingEventsLL);
    }

    private void setNavigationViewListener() {
        annLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLeftMenuSelection(8);
            }
        });
        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLeftMenuSelection(1);
            }
        });

        womensLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(6);

            }
        });


        mensLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(5);
            }
        });

        choirLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(4);
            }
        });

        profileLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(3);
            }
        });
        adminRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(2);
            }
        });
        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(7);
            }
        });
        contactLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(9);
            }
        });
        teenLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(10);
            }
        });
        donationsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(11);
            }
        });
        gloryLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(12);
            }
        });
        upcomingEventsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(14);
            }
        });
        aboutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeftMenuSelection(13);
            }
        });
    }

     public void setLeftMenuSelection(int pos) {
        txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
        txtAnnTV.setTextColor(getResources().getColor(R.color.colorBlack));
        txtChoirTV.setTextColor(getResources().getColor(R.color.colorBlack));
        txtMensTV.setTextColor(getResources().getColor(R.color.colorBlack));
        txtWomensTV.setTextColor(getResources().getColor(R.color.colorBlack));
        txtAdminTV.setTextColor(getResources().getColor(R.color.colorBlack));
        txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));

        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.ANN_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.PROFILE_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.CHOIR_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.ADMIN_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.MENS_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.WOMEN_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.HOME_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.LOGOUT_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.GLORY_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.TEEN_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.DONATION_SEL,false);
        CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.CONTACT_SEL,false);

         CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.ABOUT_SEL,false);
         CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.EVENT_SEL,false);
         txtAboutTV.setTextColor(getResources().getColor(R.color.colorBlack));
         txtEventsTV.setTextColor(getResources().getColor(R.color.colorBlack));

         txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
         txtTeenTV.setTextColor(getResources().getColor(R.color.colorBlack));
         txtGloryTV.setTextColor(getResources().getColor(R.color.colorBlack));
         txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBlack));

        if (pos == 1) {
            drawer_layout.closeDrawer(GravityCompat.START);
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBg));
            Intent intent=new Intent(mActivity, HomeActivity.class);
            CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.HOME_SEL,true);
            startActivity(intent);
            finish();

        } else if (pos == 2) {
            drawer_layout.closeDrawer(GravityCompat.START);
            txtAdminTV.setTextColor(getResources().getColor(R.color.colorBg));
            CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.ADMIN_SEL,true);
            Intent intent=new Intent(mActivity, AdminActivity.class);
            startActivity(intent);
            finish();
        } else if (pos == 3) {
            CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.PROFILE_SEL,true);
            drawer_layout.closeDrawer(GravityCompat.START);
            Intent intent=new Intent(mActivity, ProfileActivity.class);
            startActivity(intent);
            finish();
        } else if (pos == 4) {
            drawer_layout.closeDrawer(GravityCompat.START);
            txtChoirTV.setTextColor(getResources().getColor(R.color.colorBg));
            CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.CHOIR_SEL,true);
//            Intent intent=new Intent(mActivity, ChoirActivity.class);
//            startActivity(intent);
//            finish();
        } else if (pos == 5) {
            drawer_layout.closeDrawer(GravityCompat.START);
            txtMensTV.setTextColor(getResources().getColor(R.color.colorBg));
            CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.MENS_SEL,true);
            Intent intent=new Intent(mActivity, MensMinistryActivity.class);
            startActivity(intent);
            finish();
        } else if (pos == 6) {
            drawer_layout.closeDrawer(GravityCompat.START);
            txtWomensTV.setTextColor(getResources().getColor(R.color.colorBg));
            CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.WOMEN_SEL,true);
            Intent intent=new Intent(mActivity, WomensMinistryActivity.class);
            startActivity(intent);
            finish();
        } else if (pos == 7) {
            CandaceyoungPreferences.writeBoolean(mActivity,CandaceyoungPreferences.LOGOUT_SEL,true);
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBg));
            performLogoutClick();
        }else if (pos == 8) {
            txtAnnTV.setTextColor(getResources().getColor(R.color.colorBg));
            CandaceyoungPreferences.writeBoolean(mActivity, CandaceyoungPreferences.ANN_SEL, true);
            drawer_layout.closeDrawer(GravityCompat.START);
            Intent intent = new Intent(mActivity, AnnouncementActivity.class);
            startActivity(intent);
            finish();

        }
        else if (pos == 9) {
            drawer_layout.closeDrawer(GravityCompat.START);
            CandaceyoungPreferences.writeBoolean(mActivity, CandaceyoungPreferences.CONTACT_SEL, true);
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBg));
            Intent intent = new Intent(mActivity, ContactUsActivity.class);
            startActivity(intent);
            finish();
        } else if (pos == 10) {
            drawer_layout.closeDrawer(GravityCompat.START);
            CandaceyoungPreferences.writeBoolean(mActivity, CandaceyoungPreferences.TEEN_SEL, true);
            txtTeenTV.setTextColor(getResources().getColor(R.color.colorBg));
            Intent intent = new Intent(mActivity, TeenActivity.class);
            startActivity(intent);
            finish();
        } else if (pos == 11) {
            txtDonationsTV.setTextColor(getResources().getColor(R.color.colorBg));
            CandaceyoungPreferences.writeBoolean(mActivity, CandaceyoungPreferences.DONATION_SEL, true);
            drawer_layout.closeDrawer(GravityCompat.START);
            Intent intent = new Intent(mActivity, DonationActivity.class);
            startActivity(intent);
            finish();

        }else if (pos == 12) {
            txtGloryTV.setTextColor(getResources().getColor(R.color.colorBg));
            CandaceyoungPreferences.writeBoolean(mActivity, CandaceyoungPreferences.GLORY_SEL, true);
            drawer_layout.closeDrawer(GravityCompat.START);
            Intent intent = new Intent(mActivity, GloryActivity.class);
            startActivity(intent);
            finish();

        }
        else if (pos == 13) {
            txtAboutTV.setTextColor(getResources().getColor(R.color.colorBg));
            CandaceyoungPreferences.writeBoolean(mActivity, CandaceyoungPreferences.ABOUT_SEL, true);
//            drawer_layout.closeDrawer(GravityCompat.START);
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://privacy.restoredglory.org"));
            browserIntent.setData(Uri.parse("http://privacy.restoredglory.org"));
            startActivity(browserIntent);

        }else if (pos == 14) {
            txtEventsTV.setTextColor(getResources().getColor(R.color.colorBg));
            CandaceyoungPreferences.writeBoolean(mActivity, CandaceyoungPreferences.EVENT_SEL, true);
            drawer_layout.closeDrawer(GravityCompat.START);
            Intent intent = new Intent(mActivity, GetEventsActivity.class);
            startActivity(intent);
            finish();

        }

    }

    private void performLogoutClick() {
        showSignoutAlert(mActivity, getString(R.string.logout_sure));

    }

    public void showSignoutAlert(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.signout_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnNo = alertDialog.findViewById(R.id.btnNo);
        TextView btnYes = alertDialog.findViewById(R.id.btnYes);
        txtMessageTV.setText(strMessage);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }

    private void logout() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeLogoutApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mlogoutParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(mlogoutParam()).enqueue(new Callback<LogoutModel>() {
            @Override
            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                LogoutModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    SharedPreferences preferences = CandaceyoungPreferences.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    mActivity.onBackPressed();
                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<LogoutModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());


            }
        });
    }

    @OnClick({R.id.imgSearchIV, R.id.imgMenuIV,R.id.imgCancelIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSearchIV:
                searchRL.setVisibility(View.VISIBLE);
                txtHeadingTV.setVisibility(View.GONE);
                imgMenuIV.setVisibility(View.GONE);
                imgSearchIV.setVisibility(View.GONE);
                performSearchClick();
                break;
            case R.id.imgMenuIV:
//                mSimpleSideDrawer.openLeftSide();
                drawerOpenClick();
                break;
            case R.id.imgCancelIV:
                performCancelClick();
                break;
        }
    }
    private void drawerOpenClick() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer_layout, 1, 0) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                content.setTranslationX(slideX);
            }
        };

        drawer_layout.addDrawerListener(actionBarDrawerToggle);
        drawer_layout.openDrawer(Gravity.LEFT);
    }


    private void performCancelClick() {
        hideKeyBoard(mActivity,getCurrentFocus());
        searchRL.setVisibility(View.GONE);
        txtHeadingTV.setVisibility(View.VISIBLE);
        imgMenuIV.setVisibility(View.VISIBLE);
        imgSearchIV.setVisibility(View.VISIBLE);
        editSearchET.setText("");
        getChoirList();
    }

    private void SearchItem() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // filter your list from your input
                filter(s.toString());
                //you can use runnable postDelayed like 500 ms to delay search text
            }
        });
        editSearchET.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f
                    if (mRecentItemsList.size()==0){
                        showToast(mActivity,"No Result found");

                    }
                    hideKeyBoard(mActivity,getCurrentFocus());
                    return true;
                }
                return false;
            }
        });
    }

    void filter(String text) {
        mRecentItemsList = new ArrayList();
        for (DescriptionModel.Datum d : mDescCategoryList) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.getTitle().toLowerCase().contains(text.toLowerCase())) {
                mRecentItemsList.add(d);
            }
        }
        //update recyclerview
        if (mDescCategoryList.size()>0){
        mChoirAdapter.updateList(mRecentItemsList);
    }
//        if (mRecentItemsList.size()==0){
//            showToast(mActivity,"No Result found");
//
//        }
    }

    public void performSearchClick() {
        SearchItem();
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity, HomeActivity.class));
        finish();
    }
}

